package com.android.adapters;

import com.android.models.Student;
import com.android.studentsmanager.R;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterStudent extends ArrayAdapter<Object> {
	private Context context;
	private ArrayList<?> listData;

	public AdapterStudent(Context context, ArrayList<Object> data) {
		super(context, R.layout.row_student, data);
		// TODO Auto-generated constructor stub
		this.context = context;
		listData = data;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Object data = listData.get(position);
		row = inflater.inflate(R.layout.row_student, parent, false);
		Student std = (Student) data;
		TextView tv_name = (TextView) row.findViewById(R.id.tv_name);
		TextView tv_address = (TextView) row.findViewById(R.id.tv_address);
		TextView tv_email = (TextView) row.findViewById(R.id.tv_email);
		tv_name.setText(std.getName());
		tv_address.setText(std.getAddress());
		tv_email.setText(std.getEmail());
		return row;
	}
}

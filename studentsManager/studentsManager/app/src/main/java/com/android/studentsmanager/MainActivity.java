package com.android.studentsmanager;

import java.util.ArrayList;


import com.android.adapters.AdapterStudent;
import com.android.db.DBHandler;
import com.android.models.Student;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity implements OnClickListener,
		OnItemClickListener {
	private Context context;
	private ListView lv_student;
	private AdapterStudent adapter;
	private ArrayList<Object> list;
	private Button btn_new;
	private EditText edt_search;
	private Button btn_search;
	private DBHandler db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;
		lv_student = (ListView) findViewById(R.id.lv_student);
		lv_student.setOnItemClickListener(this);
		btn_new = (Button) findViewById(R.id.btn_student);
		btn_new.setOnClickListener(this);
		
		db = new DBHandler(context);
		list = db.getAllStudents();
		adapter = new AdapterStudent(context, list);
		lv_student.setAdapter(adapter);
		
		edt_search = (EditText) findViewById(R.id.edit_search);
		edt_search.setOnClickListener(this);
		btn_search = (Button) findViewById(R.id.btn_search);
		btn_search.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btn_new) {
			Intent intent = new Intent(context, InfoActivity.class);
			startActivity(intent);
		} else if (v == btn_search) {
			list = db.searchStudents(edt_search.getText().toString());
			adapter = new AdapterStudent(context, list);
			lv_student.setAdapter(adapter);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
		// TODO Auto-generated method stub
		if (arg0 == lv_student) {
			Student student = (Student) list.get(index);
			Log.e("id", student.getId() + "");
			Intent intent = new Intent(context, InfoActivity.class);
			intent.putExtra("id", student.getId() + "");
			intent.putExtra("name", student.getName());
			intent.putExtra("address", student.getAddress());
			intent.putExtra("email", student.getEmail());
			startActivity(intent);
		}
	}

}

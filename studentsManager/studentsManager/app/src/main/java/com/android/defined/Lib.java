package com.android.defined;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;


public class Lib {
	public final static int ACTION_GET_PHOTO = 2390;


	public static void pushInfos(Context context, String key, String value) {
		// sharedPerferencesExecutor.save(OBJECT_NAME, obj);
		SharedPreferences appSharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context.getApplicationContext());
		Editor prefsEditor = appSharedPrefs.edit();
		prefsEditor.putString(key, value);
		prefsEditor.commit();
	}

	public static String getInfos(Context context, String key) {
		SharedPreferences appSharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context.getApplicationContext());
		String value = appSharedPrefs.getString(key, "");
		return value;
	}
}

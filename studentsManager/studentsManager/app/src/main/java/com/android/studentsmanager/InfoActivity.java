package com.android.studentsmanager;

import com.android.db.DBHandler;
import com.android.models.Student;
import com.android.defined.Lib;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class InfoActivity extends Activity implements OnClickListener {
	private Context context;
	private EditText edt_name, edt_address, edt_email;
	private Button btn_add, btn_update, btn_delete;
	private Bundle extras = null;
	private int id = 0;
	private ImageView iv_share_fb;
	private Button btn_view_address;
	private DBHandler db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		db = new DBHandler(context);
		setContentView(R.layout.activity_info);
		edt_name = (EditText) findViewById(R.id.edt_name);
		edt_name.setOnClickListener(this);
		edt_address = (EditText) findViewById(R.id.edt_address);
		edt_address.setOnClickListener(this);
		edt_email = (EditText) findViewById(R.id.edt_email);
		edt_email.setOnClickListener(this);
		btn_add = (Button) findViewById(R.id.btn_add);
		btn_add.setOnClickListener(this);
		btn_update = (Button) findViewById(R.id.btn_update);
		btn_update.setOnClickListener(this);
		btn_delete = (Button) findViewById(R.id.btn_delete);
		btn_delete.setOnClickListener(this);
		iv_share_fb = (ImageView) findViewById(R.id.iv_share_facebook);
		iv_share_fb.setOnClickListener(this);
		btn_view_address = (Button) findViewById(R.id.btn_view_address);
		btn_view_address.setOnClickListener(this);
		extras = getIntent().getExtras();
		if (extras != null) {
			String str_id = extras.getString("id");
			id = Integer.parseInt(str_id);
			edt_name.setText(extras.getString("name"));
			edt_address.setText(extras.getString("address"));
			edt_email.setText(extras.getString("email"));

			btn_add.setVisibility(View.INVISIBLE);
			btn_update.setVisibility(View.VISIBLE);
			btn_delete.setVisibility(View.VISIBLE);
		} else {
			btn_add.setVisibility(View.VISIBLE);
			btn_update.setVisibility(View.INVISIBLE);
			btn_delete.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btn_add) {
			db.addStudent(new Student(edt_name.getText().toString(),
					edt_address.getText().toString(), edt_email.getText().toString()));
			Intent intent = new Intent(context, MainActivity.class);
			startActivity(intent);
			finish();
		} else if (v == btn_update) {
			Student std = new Student(id, edt_name.getText().toString(),
					edt_address.getText().toString(), edt_email.getText().toString());
			db.updateStudent(std);
			Intent intent = new Intent(context, MainActivity.class);
			startActivity(intent);
			finish();
		} else if (v == btn_delete) {
			Student std = new Student(id, edt_name.getText().toString(),
					edt_address.getText().toString(), edt_email.getText().toString());
			db.deleteStudent(std);
			Intent intent = new Intent(context, MainActivity.class);
			startActivity(intent);
			finish();
		} else if (v == iv_share_fb) {
			loginToFacebook();
		} else if (v == btn_view_address) {
			Intent intent = new Intent(context, AddressActivity.class);
			intent.putExtra("address", edt_address.getText().toString());
			intent.putExtra("name", edt_name.getText().toString());
			startActivity(intent);
		}
	}

	public void loginToFacebook() {
		String access_token = Lib.getInfos(context, "access_token");
		String access_expires = Lib.getInfos(context, "access_expires");
		if (access_token != null && access_token != "") {

		}
		if (access_expires != "" && access_expires != "0") {

		}


	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	}

	/**
	 * Function to post to facebook wall
	 * */
	public void postToWall() {
		// post on user's wall.
		Bundle parameters = new Bundle();
		parameters.putString("link", "http://localhost");
		parameters.putString("name", "Student");
		// parameters.putString("caption", "your subtitle");
		parameters.putString("description", "Name: "
				+ edt_name.getText().toString() + "\n Address: "
				+ edt_address.getText().toString() + "\n Email: "
				+ edt_email.getText().toString());


	}

}

package com.android.models;

public class Student {
	private int _id;
	private String _name;
	private String _address;
	private String _email;

	public Student() {

	}

	public Student(int id, String name, String address, String email) {
		this._id = id;
		this._name = name;
		this._address = address;
		this._email = email;
	}

	public Student(String name, String address, String email) {
		this._name = name;
		this._address = address;
		this._email = email;
	}

	public int getId() {
		return this._id;
	}

	public void setId(int id) {
		this._id = id;
	}

	public String getName() {
		return this._name;
	}

	public void setName(String name) {
		this._name = name;
	}

	public String getAddress() {
		return this._address;
	}

	public void setAddress(String address) {
		this._address = address;
	}

	public String getEmail() {
		return this._email;
	}

	public void setEmail(String email) {
		this._email = email;
	}
}

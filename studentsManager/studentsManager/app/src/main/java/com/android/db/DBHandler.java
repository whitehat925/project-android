package com.android.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.models.Student;

public class DBHandler extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "studentManager";
	private static final String TABLE_CONTACTS = "students";
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_ADDRESS = "address";
	private static final String KEY_EMAIL = "email";

	public DBHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
				+ KEY_ADDRESS + " TEXT," + KEY_EMAIL + " TEXT" + ")";
		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
		onCreate(db);
	}

	public void addStudent(Student student) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, student.getName());
		values.put(KEY_ADDRESS, student.getAddress());
		values.put(KEY_EMAIL, student.getEmail());
		db.insert(TABLE_CONTACTS, null, values);
		db.close();
	}

	public int updateStudent(Student student) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, student.getName());
		values.put(KEY_ADDRESS, student.getAddress());
		values.put(KEY_EMAIL, student.getEmail());
		return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
				new String[] { String.valueOf(student.getId()) });
	}

	public void deleteStudent(Student student) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
				new String[] { String.valueOf(student.getId()) });
		db.close();
	}

	public int getStudentCount() {
		String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		return cursor.getCount();
	}

	public Student getStudent(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
				KEY_NAME, KEY_ADDRESS, KEY_EMAIL }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Student student = new Student(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3));
		return student;
	}

	public ArrayList<Object> getAllStudents() {
		ArrayList<Object> studentList = new ArrayList<Object>();
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Student student = new Student();
				student.setId(Integer.parseInt(cursor.getString(0)));
				student.setName(cursor.getString(1));
				student.setAddress(cursor.getString(2));
				student.setEmail(cursor.getString(3));
				studentList.add(student);
			} while (cursor.moveToNext());
		}
		return studentList;
	}

	public ArrayList<Object> searchStudents(String KEY, String str) {
		ArrayList<Object> studentList = new ArrayList<Object>();
		String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY + " LIKE" + "%" + str + "%";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Student student = new Student();
				student.setId(Integer.parseInt(cursor.getString(0)));
				student.setName(cursor.getString(1));
				student.setAddress(cursor.getString(2));
				student.setEmail(cursor.getString(3));
				studentList.add(student);
			} while (cursor.moveToNext());
		}
		return studentList;
	}

	public ArrayList<Object> searchStudents(String str) {
		ArrayList<Object> studentList = new ArrayList<Object>();
		String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE "
				+ KEY_NAME + " LIKE '%" + str + "%' OR "+KEY_ADDRESS+" LIKE '%"+str+"%'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Student student = new Student();
				student.setId(Integer.parseInt(cursor.getString(0)));
				student.setName(cursor.getString(1));
				student.setAddress(cursor.getString(2));
				student.setEmail(cursor.getString(3));
				studentList.add(student);
			} while (cursor.moveToNext());
		}
		return studentList;
	}
}

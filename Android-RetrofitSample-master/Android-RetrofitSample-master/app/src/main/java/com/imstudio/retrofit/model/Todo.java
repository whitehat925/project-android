package com.imstudio.retrofit.model;

import com.google.gson.Gson;

public final class Todo {

    private String id;
    private String title;
    private boolean completed;
    private String userId;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}

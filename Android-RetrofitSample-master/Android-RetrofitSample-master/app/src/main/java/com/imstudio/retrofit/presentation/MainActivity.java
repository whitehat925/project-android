package com.imstudio.retrofit.presentation;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.imstudio.retrofit.R;
import com.imstudio.retrofit.model.Todo;
import com.imstudio.retrofit.network.CoreCallBack;
import com.imstudio.retrofit.network.NetworkProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.text_username)
    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        NetworkProvider.self().getTodo("1", new CoreCallBack.With<Todo>() {
            @Override
            public void run(Todo todo) {
                if (todo != null)
                    textView.setText(todo.toString());
            }
        });
    }
}


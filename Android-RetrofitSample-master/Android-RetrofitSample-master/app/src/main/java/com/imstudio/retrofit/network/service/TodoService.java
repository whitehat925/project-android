package com.imstudio.retrofit.network.service;

import com.imstudio.retrofit.model.Todo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Philippe on 02/03/2018.
 */

public interface TodoService {
    @GET("todos/{id}")
    Call<Todo> getTodo(@Path("id") String userId);
}

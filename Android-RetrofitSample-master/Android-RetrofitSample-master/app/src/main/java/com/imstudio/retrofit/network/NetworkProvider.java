package com.imstudio.retrofit.network;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.imstudio.retrofit.model.Todo;
import com.imstudio.retrofit.network.service.TodoService;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class NetworkProvider {

    private static final String TAG = NetworkProvider.class.getSimpleName();

    private static volatile NetworkProvider mInstance = null;

    private Retrofit retrofit;

    private NetworkProvider() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .client(new OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();
    }

    public static NetworkProvider self() {
        if (mInstance == null)
            mInstance = new NetworkProvider();
        return mInstance;
    }

    private <T> T getService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }

    public void getTodo(String id, final CoreCallBack.With<Todo> coreCallBack) {
        getService(TodoService.class).getTodo(id).enqueue(new Callback<Todo>() {
            @Override
            public void onResponse(Call<Todo> call, Response<Todo> response) {
                if (coreCallBack != null) {
                    coreCallBack.run(response.body());
                }
            }

            @Override
            public void onFailure(Call<Todo> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}
package com.example.sqlite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sqlite.model.CongViec;
import com.example.sqlite.model.InterfaceRecycler;
import com.example.sqlite.model.RecyclerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements InterfaceRecycler{

    Database database;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<CongViec> congViecArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        congViecArrayList = new ArrayList<>();
        //tao database
        database = new Database(MainActivity.this, "ManagerWork.sqlite", null,1);
        //tao bang Cong viec
        database.QuerryData("CREATE TABLE IF NOT EXISTS CongViec(Id INTEGER PRIMARY KEY AUTOINCREMENT, TenCV VARCHAR(50))");
        recyclerAdapter = new RecyclerAdapter(congViecArrayList, MainActivity.this,  this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerAdapter);

        GetDataWork();
    }

    private void GetDataWork() {
        congViecArrayList.clear();
        Cursor cursor = database.GetData("Select * from CongViec");
        while (cursor.moveToNext()){
            String name = cursor.getString(1);
            int id = cursor.getInt(0);
            CongViec congViec = new CongViec.CongViecBuilder()
                    .setIDCongViec(id)
                    .setTenCongViec(name)
                    .buid();
            congViecArrayList.add(congViec);
        }
        recyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_data, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menuAdd){
            DialogAdd();
        }
        return super.onOptionsItemSelected(item);
    }

    private void DialogAdd() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_data_layout);
        dialog.show();

        EditText edit_add_name = (EditText) dialog.findViewById(R.id.Edit_workName);
        Button btn_add = (Button) dialog.findViewById(R.id.btn_add_item);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_add_name.length() != 0){
                    database.QuerryData("INSERT INTO CongViec VALUES(null,'"+edit_add_name.getText().toString()+"')");
                    dialog.dismiss();
                    GetDataWork();
                }
            }
        });
    }

    @Override
    public void onClickItem(int position) {
        database.QuerryData("Delete from CongViec Where Id ="+congViecArrayList.get(position).getIDCongViec());
        congViecArrayList.remove(position);
        recyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickUpdate(int position) {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_data_layout);
        dialog.show();

        EditText edit_add_name = (EditText) dialog.findViewById(R.id.Edit_workName);
        Button btn_update = (Button) dialog.findViewById(R.id.btn_add_item);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        edit_add_name.setText(congViecArrayList.get(position).getTenCongViec());
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.QuerryData("Update CongViec set TenCV = '"+edit_add_name.getText().toString()+"' where Id = "+ congViecArrayList.get(position).getIDCongViec());
                Toast.makeText(MainActivity.this, "Update complete", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                GetDataWork();
            }
        });
    }
}
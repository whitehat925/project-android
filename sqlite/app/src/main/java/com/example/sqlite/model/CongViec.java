package com.example.sqlite.model;

public class CongViec {
    private int IDCongViec;
    private String TenCongViec;


    public int getIDCongViec() {
        return IDCongViec;
    }

    public String getTenCongViec() {
        return TenCongViec;
    }

    public CongViec() {
    }

    public CongViec(int IDCongViec, String tenCongViec) {
        this.IDCongViec = IDCongViec;
        TenCongViec = tenCongViec;
    }

    public static class CongViecBuilder{
        private int IDCongViec;
        private String TenCongViec;

        public CongViecBuilder(int IDCongViec, String tenCongViec) {
            this.IDCongViec = IDCongViec;
            this.TenCongViec = tenCongViec;
        }

        public CongViecBuilder() {
        }

        public CongViecBuilder setIDCongViec(int idCongViec){
            this.IDCongViec = idCongViec;
            return this;
        }

        public CongViecBuilder setTenCongViec(String tenCongViec){
            this.TenCongViec = tenCongViec.isEmpty()?"free time": tenCongViec;
            return this;
        }

        public CongViec buid(){
            return new CongViec(IDCongViec, TenCongViec);
        }
    }
}

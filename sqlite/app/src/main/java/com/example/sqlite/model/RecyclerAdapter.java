package com.example.sqlite.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlite.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CongViec> congViecList;
    private Context context;
    private InterfaceRecycler interfaceRecycler;

    public RecyclerAdapter(List<CongViec> congViecList, Context context, InterfaceRecycler interfaceRecycler) {
        this.congViecList = congViecList;
        this.context = context;
        this.interfaceRecycler = interfaceRecycler;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_data_work_layout, null);

        return new RecyclerHolder(view, interfaceRecycler);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RecyclerHolder recyclerHolder = (RecyclerHolder) holder;

        recyclerHolder.tv_id.setText(congViecList.get(position).getIDCongViec()+"");
        recyclerHolder.tv_name.setText(congViecList.get(position).getTenCongViec());
        recyclerHolder.img_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceRecycler.onClickItem(holder.getAdapterPosition());
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceRecycler.onClickUpdate(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return congViecList.size();
    }
}

class RecyclerHolder extends RecyclerView.ViewHolder{

    public InterfaceRecycler interfaceRecycler;
    @BindView(R.id.tv_id)
    TextView tv_id;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.img_delete)
    ImageView img_del;
    public RecyclerHolder(@NonNull View itemView, InterfaceRecycler interfaceRecycler) {
        super(itemView);
        this.interfaceRecycler = interfaceRecycler;
        ButterKnife.bind(this, itemView);
    }
}

package com.example.random;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn;
        TextView txt;
        EditText so1,so2;
        txt = (TextView) findViewById(R.id.txt_show);
        btn = (Button) findViewById(R.id.btn);
        so1 = (EditText)findViewById(R.id.txt_max);
        so2 =(EditText) findViewById(R.id.txt_min);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();

                if ((so1.getText().toString()).length() == 0 || (so2.getText().toString()).length() == 0 ){
                    Toast.makeText(MainActivity.this,"Không được bỏ trống From và To",Toast.LENGTH_SHORT).show();
                }
                else {
                    int max = Integer.parseInt(so2.getText().toString());
                    int min = Integer.parseInt(so1.getText().toString());
                    if (max<=min){
                        Toast.makeText(MainActivity.this,"To phải lớn hơn From",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        int number = random.nextInt(max-min+1)+min;
                        txt.setText(String.valueOf(number));
                    }
                }
            }
        });
    }
}
package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText noiDung;
        noiDung = (EditText) findViewById(R.id.name);

        Button btn;
        btn = (Button) findViewById(R.id.btn_click);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = noiDung.getText().toString();
                Toast.makeText(MainActivity.this,"Hello "+nd,Toast.LENGTH_LONG).show();
            }
        });
    }
}
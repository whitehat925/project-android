package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    ArrayList<Contry> arrayList;
    CustomArrayAdapter customArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list_view_multi_data);

        listView =(ListView)findViewById(R.id.listView);
        arrayList = new ArrayList<>();

        arrayList.add(new Contry("Viet Nam","30000",R.drawable.vn));
        arrayList.add(new Contry("Nhat Ban","30000",R.drawable.jp));

        customArrayAdapter = new CustomArrayAdapter(MainActivity.this, R.layout.layout_list_view_multi_data, arrayList);
        listView.setAdapter(customArrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, arrayList.get(position).toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
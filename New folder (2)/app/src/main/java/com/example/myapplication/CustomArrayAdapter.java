package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter<Contry> {

    Context context;
    ArrayList <Contry> arrayList;
    int layoutResource;



    public CustomArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Contry> objects) {
        super(context, resource, objects);
        this.context=context;
        this.arrayList= objects;
        this.layoutResource= resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(layoutResource,null);
        TextView txt1 = (TextView) convertView.findViewById(R.id.textView);
        txt1.setText(arrayList.get(position).getName());

        TextView txt2 = (TextView) convertView.findViewById(R.id.textView2);

        ImageView img= (ImageView)convertView.findViewById(R.id.img);
        img.setImageResource(arrayList.get(position).getImg());
        return convertView;
    }
}

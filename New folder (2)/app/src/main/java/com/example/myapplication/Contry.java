package com.example.myapplication;

public class Contry {
    String name;
    String populate;
    int img;

    public Contry() {
    }

    public Contry(String name, String populate, int img) {
        this.name = name;
        this.populate = populate;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPopulate() {
        return populate;
    }

    public void setPopulate(String populate) {
        this.populate = populate;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }


}

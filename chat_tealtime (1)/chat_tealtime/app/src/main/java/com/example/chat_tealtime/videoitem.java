package com.example.chat_tealtime;

public class videoitem {

    public String Videourl;
    public String video_name;
    public String video_text;

    public videoitem() {
    }

    public videoitem(String videourl, String video_name, String video_text) {
        this.Videourl = videourl;
        this.video_name = video_name;
        this.video_text = video_text;
    }

    public String getVideourl() {
        return Videourl;
    }

    public void setVideourl(String videourl) {
        this.Videourl = videourl;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_text() {
        return video_text;
    }

    public void setVideo_text(String video_text) {
        this.video_text = video_text;
    }
}

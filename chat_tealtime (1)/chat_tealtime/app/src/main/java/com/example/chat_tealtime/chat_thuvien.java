package com.example.chat_tealtime;

import android.graphics.Bitmap;

public class chat_thuvien {
    private Bitmap hinh;

    public chat_thuvien(Bitmap hinh) {
        this.hinh = hinh;
    }

    public Bitmap getHinh() {
        return hinh;
    }

    public void setHinh(Bitmap hinh) {
        this.hinh = hinh;
    }
}

package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class game_3cayonline_activity extends AppCompatActivity {
    Button btn_choi, btn_latbai, btn_lichsu, btn_thoat;
    ImageView card_1_player1, card_1_player2, card_2_player2, card_2_player1, card_3_player1, card_3_player2;
    TextView name_player1, name_player2, diem1, diem2, sum1, sum2;
    String name, SDT;
    int check_player = 0, dem = 0, sum = -1, check_sum1 = -1, check_sum2 = -1;
    int score1_1 = 0, score1_2 = 0, score1_3 = 0, score2_1 = 0, score2_2 = 0, score2_3 = 0;
    ArrayList<class_bai3cay_src_score> arrayList_prepare_data;
    DatabaseReference reference;
    private static CountDownTimer count;
    int i = 19, demdata_card = 0;
    boolean count_down_check = false;
    ArrayList<String> arrayList;
    ArrayList<String> lichsu;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_3cayonline_activity);
        init();//lấy id
        getintent();// lấy data name,SDT
        reference = FirebaseDatabase.getInstance().getReference();
        prepare();
        set_data_player();//set data người chơi
        player();//thêm player
        set_data_card();// tạo data card sẵn
        set_click_Card();//click mở card
        get_data_card();// lấy data card của đối thủ
        setBtn_thoat();
        setBtn_latbai();// lật tất cả card của mk
        count_down();// tự lật bài khi hết time
        setBtn_lichsu();//lich su choi game
        get_data_lichsu();//lay data lich su
        btn_choi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBtn_choi();
            }
        });

    }

    void prepare() {// chuẩn bị bắt đầu chơi lần đầu
        card_1_player2.setEnabled(false);
        card_2_player2.setEnabled(false);
        card_3_player2.setEnabled(false);
        btn_latbai.setEnabled(false);
        btn_choi.setEnabled(false);
        reference.child("Game").child("Bai3cay").child("databai3cay").removeValue();
    }// chuẩn bị bắt đầu chơi lần đầu

    void setBtn_choi() {//set btn chơi, xóa để lấy hàm remove firebase
        btn_latbai.setEnabled(true);
        reference.child("Game").child("Bai3cay").child("databai3cay").removeValue();
    }//set btn chơi, xóa để lấy hàm remove firebase

    void setBtn_thoat() {// btn thoát
        btn_thoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }//thoát

    void init() {
        btn_choi = (Button) findViewById(R.id.btn_choigame3cay);
        btn_latbai = (Button) findViewById(R.id.btn_Latbai);
        btn_lichsu = (Button) findViewById(R.id.btn_lichsu_game3cay);
        btn_thoat = (Button) findViewById(R.id.btn_thoat_game_3cay);
        card_1_player1 = (ImageView) findViewById(R.id.card_1_player1);
        card_1_player2 = (ImageView) findViewById(R.id.card_1_player2);
        card_2_player1 = (ImageView) findViewById(R.id.card_2_player1);
        card_2_player2 = (ImageView) findViewById(R.id.card_2_player2);
        card_3_player1 = (ImageView) findViewById(R.id.card_3_player1);
        card_3_player2 = (ImageView) findViewById(R.id.card_3_player2);
        name_player1 = (TextView) findViewById(R.id.name_player1_gamebai3cay);
        name_player2 = (TextView) findViewById(R.id.name_player2_gamebai3cay);
        diem1 = (TextView) findViewById(R.id.score_player1_gamebai3cay);
        diem2 = (TextView) findViewById(R.id.score_player2_gamebai3cay);
        sum1 = (TextView) findViewById(R.id.txt1);
        sum2 = (TextView) findViewById(R.id.txtban);


    }// chiếu

    void setBtn_lichsu() {
        btn_lichsu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lichsu = new ArrayList<>();
                for (int i = arrayList.size() - 1; i >= 0; i--) {
                    lichsu.add(arrayList.get(i) + "");

                }
                //Toast.makeText(game_3cayonline_activity.this,lichsu.get(0),Toast.LENGTH_SHORT).show();

                //Toast.makeText(game_3cayonline_activity.this,"OK",Toast.LENGTH_SHORT).show();

                Dialog dialog = new Dialog(game_3cayonline_activity.this);
                dialog.setContentView(R.layout.layout_lichsu_gamebai3cay);
                ListView listView = (ListView) dialog.findViewById(R.id.lichsu_gamebai3cay);

                adapter = new ArrayAdapter(game_3cayonline_activity.this, android.R.layout.simple_list_item_1, lichsu);
                listView.setAdapter(adapter);
                dialog.show();


            }
        });
    }// click lịch sử

    void get_data_lichsu() {// lấy data lịch sử
        arrayList = new ArrayList<>();
        reference.child("Game").child("Bai3cay").child("lichsu").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                class_lichsu_gamebai3cay lichsu_gamebai3cay = snapshot.getValue(class_lichsu_gamebai3cay.class);
                //Toast.makeText(game_3cayonline_activity.this,lichsu_gamebai3cay.str,Toast.LENGTH_SHORT).show();
                arrayList.add(new String(lichsu_gamebai3cay.str));

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }// lấy data lịch sử

    void getintent() {
        Intent getintent = getIntent();
        name = getintent.getStringExtra("name");
        SDT = getintent.getStringExtra("SDT");
    }// lấy data gửi intent

    void setBtn_latbai() {
        btn_latbai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count.cancel();
                btn_latbai.setText("Lật bài");
                btn_latbai.setEnabled(false);
                card_1_player2.performClick();
                card_2_player2.performClick();
                card_3_player2.performClick();
            }
        });


    }// lật all bài

    void set_data_player() {
        reference.child("Game").child("Bai3cay").child("Player").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                check_player = check_player + 1;
                if (check_player > 2) {
                    return;
                }
                //set tên và dữ liệu người chơi
                Player_caro player = snapshot.getValue(Player_caro.class);
                if (check_player == 1) {

                    name_player1.setText(player.name);
                    diem1.setText(diem1.getText().toString() + player.diem + "");
                }
                if (check_player == 2) {
                    Toast.makeText(game_3cayonline_activity.this, "Sẵn Sàng", Toast.LENGTH_LONG).show();

                    name_player2.setText(player.name);

                    card_1_player2.setEnabled(true);// bắt đàu chơi khi có đủ người
                    card_2_player2.setEnabled(true);
                    card_3_player2.setEnabled(true);
                    btn_latbai.setEnabled(true);

                    diem2.setText(player.diem + "" + diem2.getText().toString());
                }


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                //dóng màn hình ở các thiết bị đang tham gia
                finish();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }// lấy data khi có máy login

    void player() {
        Player_caro player = new Player_caro(name, SDT, 1 + "", 100);
        //reference.child("Game").child("Caro").child("Player").removeValue();
        reference.child("Game").child("Bai3cay").child("Player").push().setValue(player);
    }//push player lên database

    void set_data_card() {
        arrayList_prepare_data = new ArrayList<>();
        arrayList_prepare_data.add(new class_bai3cay_src_score(0, "card_10_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(1, "card_at_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(2, "card_2_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(3, "card_3_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(4, "card_4_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(5, "card_5_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(6, "card_6_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(7, "card_7_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(8, "card_8_bich"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(9, "card_9_bich"));

        arrayList_prepare_data.add(new class_bai3cay_src_score(0, "card_10_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(1, "card_at_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(2, "card_2_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(3, "card_3_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(4, "card_4_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(5, "card_5_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(6, "card_6_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(7, "card_7_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(8, "card_8_co"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(9, "card_9_co"));

        arrayList_prepare_data.add(new class_bai3cay_src_score(0, "card_10_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(1, "card_at_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(2, "card_2_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(3, "card_3_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(4, "card_4_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(5, "card_5_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(6, "card_6_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(7, "card_7_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(8, "card_8_tep"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(9, "card_9_tep"));

        arrayList_prepare_data.add(new class_bai3cay_src_score(0, "card_10_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(1, "card_at_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(2, "card_2_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(3, "card_3_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(4, "card_4_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(5, "card_5_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(6, "card_6_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(7, "card_7_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(8, "card_8_ro"));
        arrayList_prepare_data.add(new class_bai3cay_src_score(9, "card_9_ro"));


    }// data card có sẵn

    void set_click_Card() {
        card_1_player2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_latbai.setEnabled(false);

                card_1_player2.setEnabled(false);
                dem += 1;
                int r = random_card();
                score1_1 = set_card_img(r, card_1_player2);
                if (dem == 3) {
                    sum = score1_1 + score1_2 + score1_3;
                    if (sum >= 10) {
                        sum = sum % 10;
                    }
                    sum2.setText(sum2.getText() + "          " + sum);
                    check_sum1 = sum;
                    if (check_sum2 != -1) {
                        check_win();
                        if (count_down_check) {
                            count.cancel();
                        }

                    }
                }
                class_game_bai3cay_data cay_data = new class_game_bai3cay_data(name, 1, r, sum);
                reference.child("Game").child("Bai3cay").child("databai3cay").push().setValue(cay_data);


            }
        });
        card_2_player2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_latbai.setEnabled(false);

                card_2_player2.setEnabled(false);
                dem += 1;
                int r = random_card();
                score1_2 = set_card_img(r, card_2_player2);
                if (dem == 3) {
                    sum = score1_1 + score1_2 + score1_3;
                    if (sum >= 10) {
                        sum = sum % 10;
                    }
                    sum2.setText(sum2.getText() + "          " + sum);
                    check_sum1 = sum;
                    if (check_sum2 != -1) {
                        check_win();
                        if (count_down_check) {
                            count.cancel();
                        }
                    }
                }
                class_game_bai3cay_data cay_data = new class_game_bai3cay_data(name, 2, r, sum);
                reference.child("Game").child("Bai3cay").child("databai3cay").push().setValue(cay_data);


            }
        });
        card_3_player2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_latbai.setEnabled(false);

                card_3_player2.setEnabled(false);
                dem += 1;
                int r = random_card();
                score1_3 = set_card_img(r, card_3_player2);
                if (dem == 3) {
                    sum = score1_1 + score1_2 + score1_3;
                    if (sum >= 10) {
                        sum = sum % 10;
                    }
                    sum2.setText(sum2.getText() + "          " + sum);
                    check_sum1 = sum;
                    if (check_sum2 != -1) {
                        check_win();
                        if (count_down_check) {
                            count.cancel();
                        }
                    }

                }
                class_game_bai3cay_data cay_data = new class_game_bai3cay_data(name, 3, r, sum);
                reference.child("Game").child("Bai3cay").child("databai3cay").push().setValue(cay_data);
                //Toast.makeText(game_3cayonline_activity.this,score1_1+score1_2+score1_3+"",Toast.LENGTH_SHORT).show();


            }
        });

    }// click để lật card

    void get_data_card() {
        reference.child("Game").child("Bai3cay").child("databai3cay").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                demdata_card += 1;
                int demc = 0;
                if (demdata_card == 6) {
                    if (count_down_check) {
                        count.cancel();
                    }
                    btn_latbai.setText("Lật bài");
                }


                class_game_bai3cay_data data = snapshot.getValue(class_game_bai3cay_data.class);
                if (data.Name != name) {
                    if (data.vitri == 1) {
                        set_card_img(data.data_card, card_1_player1);
                    }
                    if (data.vitri == 2) {
                        set_card_img(data.data_card, card_2_player1);
                    }
                    if (data.vitri == 3) {
                        set_card_img(data.data_card, card_3_player1);
                    }
                    if (data.sum != -1) {

                        if (sum == -1) {
                            if (count_down_check == false) {
                                count.start();
                                demc += 1;
                                Log.i("count", demc + "");
                            }


                        }

                        sum1.setText(sum1.getText() + "          " + data.sum);
                        check_sum2 = data.sum;
                        if (check_sum1 != -1) {

                            check_win();
                            if (count_down_check) {
                                count.cancel();
                            }

                        }


                    }

                }


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {//reset lai tat ca data
                card_1_player2.setEnabled(true);
                card_2_player2.setEnabled(true);
                card_3_player2.setEnabled(true);
                btn_choi.setEnabled(false);
                btn_latbai.setEnabled(true);
                count_down_check = false;
                sum1.setText("Bài của đối thủ");
                sum2.setText("Bài của bạn");
                dem = 0;
                sum = -1;
                demdata_card = 0;
                i = 19;
                check_sum1 = -1;
                check_sum2 = -1;
                score1_1 = 0;
                score1_2 = 0;
                score1_3 = 0;
                card_1_player2.setImageResource(R.drawable.img_card_down);
                card_2_player2.setImageResource(R.drawable.img_card_down);
                card_3_player2.setImageResource(R.drawable.img_card_down);
                card_1_player1.setImageResource(R.drawable.img_card_down);
                card_2_player1.setImageResource(R.drawable.img_card_down);
                card_3_player1.setImageResource(R.drawable.img_card_down);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }// lấy data card đối thủ

    int random_card() {// lấy số random đẻ lấy card
        Random random = new Random();
        int r = random.nextInt(arrayList_prepare_data.size());
        return r;

    }// lấy số random đẻ lấy card

    int set_card_img(int index, ImageView imageView) {// set img card và lấy số điểm
        int id = getResources().getIdentifier(arrayList_prepare_data.get(index).SRC, "drawable", getPackageName());
        imageView.setImageResource(id);
        return arrayList_prepare_data.get(index).score;

    }// set img card và lấy số điểm

    void check_win() {
        if (count_down_check) {
            count.cancel();
        }
        if (check_sum1 > check_sum2) {
            sum2.setText("BẠN THẮNG   " + check_sum1);
            btn_choi.setText("Chơi lại");
            btn_choi.setEnabled(true);
            if (name.equals(name_player1.getText())) {
                class_lichsu_gamebai3cay lichsu = new class_lichsu_gamebai3cay(name_player1.getText() + ": Thắng\n" + name_player2.getText() + ": Thua");
                reference.child("Game").child("Bai3cay").child("lichsu").push().setValue(lichsu);
            }

        }
        if (check_sum1 < check_sum2) {
            sum2.setText("BẠN THUA   " + check_sum1);
            btn_choi.setText("Chơi lại");
            btn_choi.setEnabled(true);
            if (name.equals(name_player2.getText())) {
                class_lichsu_gamebai3cay lichsu = new class_lichsu_gamebai3cay(name_player1.getText() + ": Thua\n" + name_player2.getText() + ": Thắng");
                reference.child("Game").child("Bai3cay").child("lichsu").push().setValue(lichsu);
            }

        }
        if (check_sum1 == check_sum2) {
            sum2.setText("HÒA   " + check_sum1);
            btn_choi.setText("Chơi lại");
            btn_choi.setEnabled(true);
            if (name.equals(name_player1.getText())) {
                class_lichsu_gamebai3cay lichsu = new class_lichsu_gamebai3cay(name_player1.getText() + ": Hòa\n" + name_player2.getText() + ": Hòa");
                reference.child("Game").child("Bai3cay").child("lichsu").push().setValue(lichsu);
            }
        }
    }// kiểm tra win khi lật hết bài

    void count_down() {
        String i1 = btn_latbai.getText().toString();

        count = new CountDownTimer(20000, 1000) {// cahy 2 lan
            @Override
            public void onTick(long millisUntilFinished) {
                count_down_check = true;
                btn_latbai.setText(i1 + "    " + i);
                i--;

            }

            @Override
            public void onFinish() {
                count_down_check = false;
                Toast.makeText(game_3cayonline_activity.this, "Hết giờ", Toast.LENGTH_SHORT).show();

                if (sum == -1) {

                    btn_latbai.setText("Lật bài");
                    btn_latbai.setEnabled(false);
                    card_1_player2.performClick();
                    card_2_player2.performClick();
                    card_3_player2.performClick();
                    btn_latbai.setText(i1 + btn_latbai.getText() + demdata_card + "");
                }
            }

            //i=i-1;

        };
    }// đếm ngược khi không lật bài

    @Override
    protected void onDestroy() {
        super.onDestroy();
        reference.child("Game").child("Bai3cay").child("Player").removeValue();
    }// xóa data khi out
}
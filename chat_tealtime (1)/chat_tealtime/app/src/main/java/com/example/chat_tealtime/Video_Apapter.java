package com.example.chat_tealtime;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.List;

public class Video_Apapter extends RecyclerView.Adapter<Video_Apapter.VideoHolder> {
    List<videoitem> videoitems;


    public Video_Apapter(List<videoitem> videoitems) {
        this.videoitems = videoitems;
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
        holder.Setdatavideo(videoitems.get(position));
    }

    @Override
    public int getItemCount() {
        return videoitems.size();
    }

    static class VideoHolder extends RecyclerView.ViewHolder {
        MediaPlayer mediaPlayer;


        VideoView videoView;
        TextView txt_namevideo, txt_text_video;
        ProgressBar progressBar;
        long t1 = 0, t2 = 0;// de lay event double click
        long t3 = 0;

        public VideoHolder(@NonNull View itemView) {
            super(itemView);
            videoView = itemView.findViewById(R.id.video_view);
            txt_namevideo = itemView.findViewById(R.id.txtnammvideo);
            txt_text_video = itemView.findViewById(R.id.txt_text_video);

            progressBar = itemView.findViewById(R.id.porgressbar);
        }

        void Setdatavideo(videoitem videoitem) {
            txt_namevideo.setText(videoitem.video_name);
            txt_text_video.setText(videoitem.video_text);
            videoView.setVideoPath(videoitem.Videourl);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {

                    progressBar.setVisibility(View.GONE);
                    mp.start();
                    mediaPlayer = mp;
                    float videoratio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                    float screenratio = videoView.getWidth() / (float) videoView.getHeight();
                    float scale = videoratio / screenratio;
                    if (scale >= 1f) {
                        videoView.setScaleX(scale);
                    } else {
                        videoView.setScaleY(1f / scale);
                    }
                }
            });

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer = mp;
                    mp.start();
                }
            });

            videoView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {

                        long time = System.currentTimeMillis();// lay time de so sanh lay double click
                        t2 = time - t1;
                        if (t2 < 900) {
                            // t3=t2;
                            VIdeo_Social.like = 1;// lay trang thai like khi double click
//                            Log.i("like",VIdeo_Social.like+"");
                        }
                        t1 = time;
                        if (t3 == 0) {

                            if (mediaPlayer.isPlaying()) {

                                mediaPlayer.pause();
                            } else {
                                mediaPlayer.start();

                            }
                        }

                    } catch (Exception e) {
                    }
                    ;
                    return false;
                }
            });
        }
    }
}

package com.example.chat_tealtime;

public class mess_noidung {
    public String name;
    public   String text;
    public String link;
    public long time;

    public mess_noidung() {
    }

    public mess_noidung(String name, String text, String link,long time) {
        this.name = name;
        this.text = text;
        this.link = link;
        this.time = time;
    }

}

package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class game_caroonline extends AppCompatActivity {

    GridView banco;
    TextView name_player1, name_player2, score_player1, score_player2;

    ArrayList<String> arrayList;
    ArrayAdapter adapter;
    int[][] bancocaro = new int[25][25];
    String name, SDT;
    int STT = 0, check_player = 0, kt = 0;
    DatabaseReference reference;
    CountDownTimer count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_caroonline);
        Intent getintent = getIntent();
        name = getintent.getStringExtra("name");
        SDT = getintent.getStringExtra("SDT");
        name_player1 = (TextView) findViewById(R.id.name_player1);
        name_player2 = (TextView) findViewById(R.id.name_player2);
        score_player1 = (TextView) findViewById(R.id.score_player1);
        score_player2 = (TextView) findViewById(R.id.score_player2);
        reference = FirebaseDatabase.getInstance().getReference();
        //reference.child("Game").removeValue();


        reference.child("Game").child("Caro").child("Player").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                check_player = check_player + 1;
                if (check_player > 3) {
                    return;
                }
                //set tên và dữ liệu người chơi
                Player_caro player = snapshot.getValue(Player_caro.class);
                if (check_player == 2) {
                    setLuoichoi(1);
                    name_player1.setText(player.name);
                    score_player1.setText(score_player1.getText().toString() + player.diem + "");
                }
                if (check_player == 3) {

                    name_player2.setText(player.name);
                    score_player2.setText(player.diem + "" + score_player2.getText().toString());
                }
//                        if (check_player==0){
//                        Player_caro player = snapshot.getValue(Player_caro.class);
//                        if (player.STT.equals("1")){
//                            Player_caro player1 = new Player_caro(name,SDT,2+"",0);
//                            if (STT==0){
//                                reference.child("Game").child("Caro").child("Player").push().setValue(player1);
//                            }
//
//                            STT=1;
//
//                        }}


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                //dóng màn hình ở các thiết bị đang tham gia
                finish();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        setPlayer();
        // xử lí tạo bàn cờ
        banco = (GridView) findViewById(R.id.banco);
        arrayList = new ArrayList<>();
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < 25; j++) {
                bancocaro[i][j] = i + j + 10;
            }

        }
        for (int i = 0; i < 54; i++) {
            arrayList.add(new String("."));
        }
        for (int i = 0; i < 54; i++) {
            arrayList.add(new String("."));
        }
        adapter = new ArrayAdapter(game_caroonline.this, android.R.layout.simple_list_item_1, arrayList);
        banco.setAdapter(adapter);
        //setLuoichoi(1);
        // lấy lượt chơi trên server
        reference.child("Game").child("Caro").child("luotchoi").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                luotchoi_caro lc = snapshot.getValue(luotchoi_caro.class);
                kt = lc.luotchoi;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        banco.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {// kiểm tra XO


                if (kt == 1) {
                    if (arrayList.get(position).equals("X") || arrayList.get(position).equals("O")) {
                    }//kiểm tra đã có data hay chưa
                    else {
                        xulichoico_vitri_kihieu xl = new xulichoico_vitri_kihieu(position, "X");
                        reference.child("Game").child("Caro").child("dataBanCoCaro").push().setValue(xl);
                        // setup trên máy
                        //arrayList.set(position,"X");
                        //banco.setAdapter(adapter);
                        setLuoichoi(2);
                    }
                } else if (kt == 2) {
                    if (arrayList.get(position).equals("X") || arrayList.get(position).equals("O")) {
                    }//kiểm tra đã có data hay chưa
                    else {

                        xulichoico_vitri_kihieu xl = new xulichoico_vitri_kihieu(position, "O");
                        reference.child("Game").child("Caro").child("dataBanCoCaro").push().setValue(xl);
                        //arrayList.set(position,"O");
                        //banco.setAdapter(adapter);
                        setLuoichoi(1);
                    }
                }


            }
        });
    }

    // xử lí lượt chơi
    private void setLuoichoi(int n) {
        // thay đổi lượt chơi
        luotchoi_caro luotchuoi = new luotchoi_caro(n);
        reference.child("Game").child("Caro").child("luotchoi").setValue(luotchuoi);
        reference.child("Game").child("Caro").child("luotchoi").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                reference.child("Game").child("Caro").child("dataBanCoCaro").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                        // lấy data bàn cờ trên server
                        int x1 = 0, y1 = 0;
                        xulichoico_vitri_kihieu x = snapshot.getValue(xulichoico_vitri_kihieu.class);
                        arrayList.set(x.vitri, x.kihieu);
                        if (x.kihieu.equals("X")) {
                            x1 = chuyenmatran_x(x.vitri) + 5;
                            y1 = chuyenmatran_y(x.vitri) + 5;
                            bancocaro[x1][y1] = 1;
                            if (checkWin(x1, y1)) {

                                Toast.makeText(game_caroonline.this, "X Win", Toast.LENGTH_SHORT).show();
                            }

                        }
                        if (x.kihieu.equals("O")) {
                            // cong them 5 de khi checkwin ko bi loi
                            x1 = chuyenmatran_x(x.vitri) + 5;
                            y1 = chuyenmatran_y(x.vitri) + 5;
                            bancocaro[x1][y1] = 2;
                            if (checkWin(x1, y1)) {
                                Toast.makeText(game_caroonline.this, "O Win", Toast.LENGTH_LONG).show();
                            }
                        }
                        banco.setAdapter(adapter);
//                        if (checkWin(x1,y1)){
//                            Toast.makeText(game_caroonline.this,"Win",Toast.LENGTH_LONG).show();
//                        }
                        //Toast.makeText(game_caroonline.this,x1+"+"+y1+"",Toast.LENGTH_LONG).show();


                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        //return n;


    }

    //chuyen sang ma tran
    private int chuyenmatran_x(int position) {
        int x = position / 9;

        return x;
    }

    private int chuyenmatran_y(int position) {
        int y = position % 9;

        return y;
    }

    // thêm data player
    private void setPlayer() {
        {
            if (STT == 0) {
                Player_caro player = new Player_caro(name, SDT, 1 + "", 0);
                check_player = 1;
                //reference.child("Game").child("Caro").child("Player").removeValue();
                reference.child("Game").child("Caro").child("Player").push().setValue(player);
            }
        }
    }

    // xử lí thắng thua
    public boolean checkWin(int i, int j) {
        int d = 0, k = i, h = 0;
        // kiểm tra cot
        while (bancocaro[k][j] == bancocaro[i][j]) {
            d++;
            k++;
        }
        k = i - 1;

        while (bancocaro[k][j] == bancocaro[i][j]) {
            d++;
            k--;
        }
        if (d > 4) return true;
        d = 0;
        h = j;
        // kiểm tra hang
        while (bancocaro[i][h] == bancocaro[i][j]) {
            d++;
            h++;
        }
        h = j - 1;
        while (bancocaro[i][h] == bancocaro[i][j]) {
            d++;
            h--;
        }
        if (d > 4) return true;
        // kiểm tra đường chéo 1
        h = i;
        k = j;
        d = 0;
        while (bancocaro[i][j] == bancocaro[h][k]) {
            d++;
            h++;
            k++;
        }
        h = i - 1;
        k = j - 1;
        while (bancocaro[i][j] == bancocaro[h][k]) {
            d++;
            h--;
            k--;
        }
        if (d > 4) return true;
        //kiểm tra đường chéo 2
        h = i;
        k = j;
        d = 0;
        while (bancocaro[i][j] == bancocaro[h][k]) {
            d++;
            h++;
            k--;
        }
        h = i - 1;
        k = j + 1;
        while (bancocaro[i][j] == bancocaro[h][k]) {
            d++;
            h--;
            k++;
        }
        if (d > 4) return true;
        // nếu không đương chéo nào thỏa mãn thì trả về false.
        return false;
    }

    // khi bấm nút back
    @Override
    public void onBackPressed() {

        reference.child("Game").child("Caro").child("Player").removeValue();
        reference.child("Game").child("Caro").child("dataBanCoCaro").removeValue();
        //reference.child("Game").child("Caro").child("luotchoi").removeValue();
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < 25; j++) {
                bancocaro[i][j] = i + j + 10;
            }

        }
        check_player = 0;
        finish();

        return;
    }


    // KHI tắt app bằng nút home (1 số TH ko được gọi thì sẽ gọi onpause()
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Toast.makeText(game_caroonline.this,"Runing",Toast.LENGTH_SHORT).show();
        reference.child("Game").child("Caro").child("Player").removeValue();
        reference.child("Game").child("Caro").child("dataBanCoCaro").removeValue();
        //reference.child("Game").child("Caro").child("luotchoi").removeValue();
    }

    private void count(int time) {
        count = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //Toast.makeText(game_caroonline.this,millisUntilFinished+"",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {// khi hết 60s không vào lại app, sẽ tự động xóa data và out game
                Toast.makeText(game_caroonline.this, "Thoát Game", Toast.LENGTH_SHORT).show();

                reference.child("Game").child("Caro").child("Player").removeValue();
                reference.child("Game").child("Caro").child("dataBanCoCaro").removeValue();

            }
        };
    }


    @Override
    public void onPause() {// khi app bị tab ra
        super.onPause();
        count(60000);
        count.start();
        //reference.child("Game").child("Caro").child("luotchoi").removeValue();
    }

    @Override
    protected void onPostResume() {// khi quay lại app hủy countdown
        super.onPostResume();
        try {
            count.cancel();
        } catch (Exception e) {

        }

    }
}
package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    EditText txtuser,txtPass,txtRepeatpass;
    Button btn_login,btn_register;
    CheckBox checkBoxlogin;
    DatabaseReference databaseReference;

    int check=0;
    SharedPreferences sharedPreferences;// save data login
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Đăng Nhập");
        txtuser =(EditText) findViewById(R.id.txtUser);
        txtPass =(EditText) findViewById(R.id.txtPassword);
        btn_login= (Button) findViewById(R.id.btn_login);
        btn_register =(Button) findViewById(R.id.btn_register);
        txtRepeatpass =(EditText) findViewById(R.id.txtrepeatPassword);
        txtRepeatpass.setVisibility(View.INVISIBLE);
        // khởi tạo database
        databaseReference= FirebaseDatabase.getInstance().getReference();
        // tạo để lưu data login
        sharedPreferences = getSharedPreferences("datalogin",MODE_PRIVATE);
        checkBoxlogin =(CheckBox) findViewById(R.id.checkautologin);
        // lấy data login
        txtuser.setText(sharedPreferences.getString("SDT",""));
        txtPass.setText(sharedPreferences.getString("pass",""));
        checkBoxlogin.setChecked(sharedPreferences.getBoolean("check",false));
        Intent intent1 = getIntent();
        int s = intent1.getIntExtra("save",0);
        if (checkBoxlogin.isChecked() && (s== 0))
        {
            Intent intent2 = new Intent(MainActivity.this,Main_Screen.class);

            databaseReference.child("User").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                    // gán data trong child vào object user
                    user user = snapshot.getValue(com.example.chat_tealtime.user.class);
                    // so sánh giá trị để login
                    if  (txtuser.getText().toString().equals(user.SDT) ) {
                        intent2.putExtra("name",user.name);
                        Toast.makeText(MainActivity.this,user.name,Toast.LENGTH_LONG).show();
                        intent2.putExtra("SDT",txtuser.getText().toString());
                        //
                        startActivity(intent2);
                    }
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            this.finish();



        }
        setChecksdt();
        register();
        login();
    }

    private void setChecksdt(){
        txtuser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checksdt =0;
                if ( txtRepeatpass.getVisibility()==View.VISIBLE) {
                    checkregister();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txtRepeatpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ( txtRepeatpass.getVisibility()==View.VISIBLE) {
                    checkregister();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private boolean testlogin()// kiểm tra null
    {

        if ((txtuser.getText().toString().trim().matches("")) || (txtPass.getText().toString().trim().matches("")))
        {
            return false;
        }
        else return true;

    }
    private boolean testregister() // kiểm tra null
    {
        if ((txtuser.getText().toString().matches("")) || (txtPass.getText().toString().matches("")) || (txtRepeatpass.getText().toString().matches("")))
        {
            return false;
        }
       return true;
    }
    int checksdt = 0;
    private void checkregister(){
        databaseReference.child("User").addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                // gán data trong child vào object user
                user user = snapshot.getValue(com.example.chat_tealtime.user.class);
                // so sánh giá trị để login
                if (txtuser.getText().toString().equals(user.SDT) ) {
                    checksdt =1 ;
                    Toast.makeText(MainActivity.this, "Số điện thoại đã được sử dụng!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    
    public void register(){// đăng ký
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtRepeatpass.setVisibility(View.VISIBLE);
                checkBoxlogin.setVisibility(View.INVISIBLE);
                if (testregister()){
                    if (checksdt==0){
                        user user= new user(txtuser.getText().toString(),txtuser.getText().toString(),txtPass.getText().toString(),txtRepeatpass.getText().toString());
                        databaseReference.child("User").push().setValue(user);// push data user lên Firebase database realtime
                        Toast.makeText(MainActivity.this,"Đăng ký thành công! \n Mời bạn đăng nhập",Toast.LENGTH_LONG).show();
                        txtRepeatpass.setVisibility(View.INVISIBLE);
                        checkBoxlogin.setVisibility(View.VISIBLE);
                        txtRepeatpass.setText("");
                    }
                }
                else {
                    Toast.makeText(MainActivity.this,"Đăng ký thất bại!",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private void login(){// đăng nhâpj
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (testlogin()){// ktra trống

                    databaseReference.child("User").addChildEventListener(new ChildEventListener() {

                        @Override// đọc từng dòng trong child User
                        public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                            // gán data trong child vào object user
                            user user = snapshot.getValue(com.example.chat_tealtime.user.class);
                            // so sánh giá trị để login
                            if  (txtuser.getText().toString().equals(user.SDT) &&(txtPass.getText().toString().equals(user.password))){
                                Toast.makeText(MainActivity.this,"Đăng nhập thành công!",Toast.LENGTH_SHORT).show();
                                check = 1;

                                // lưu tài khoản, mật khẩu nếu được check
                                if (checkBoxlogin.isChecked())
                                {
                                    SharedPreferences.Editor  edit= sharedPreferences.edit();
                                    edit.putString("SDT",txtuser.getText().toString());
                                    edit.putString("pass",txtPass.getText().toString());
                                    edit.putBoolean("check",true);

                                    edit.commit();
                                }
                                else{
                                    SharedPreferences.Editor  edit= sharedPreferences.edit();
                                    edit.remove("SDT");
                                    edit.remove("pass");
                                    edit.remove("check");

                                    edit.commit();
                                }

                                // mở main activity
                                Intent intent = new Intent(MainActivity.this,Main_Screen.class);
                                intent.putExtra("name",user.name);
                                intent.putExtra("SDT",txtuser.getText().toString());

                                startActivity(intent);

                            }

                        }


                        @Override
                        public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }

                    });

                }
                else {
                    Toast.makeText(MainActivity.this,"Vui lòng điền đủ thông tin!!",Toast.LENGTH_LONG).show();

                }
                if (check==0)
                    Toast.makeText(MainActivity.this,"Sai tài khoản hoặc mật khẩu!!",Toast.LENGTH_LONG).show();
            }
        });

    }
}
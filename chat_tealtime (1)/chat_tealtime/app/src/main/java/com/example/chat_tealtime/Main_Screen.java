package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Main_Screen extends AppCompatActivity {

    ListView listView;
    DatabaseReference databaseReference;
    NotificationCompat.Builder builder = new NotificationCompat.Builder(Main_Screen.this, "mynoti");
    NotificationCompat.Builder builder1 = new NotificationCompat.Builder(Main_Screen.this, "mynoti");
    ImageView imageView;
    ArrayList<String> arrayList;
    String a, b;
    EditText txtname_city;
    SharedPreferences sharedPreferences;String city_name;
    Button btn_thoatcaidat, btn_luucaidat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__screen);
        imageView =(ImageView) findViewById(R.id.welcome);

        set_click_welcome();

        Intent intent = getIntent();
        sharedPreferences = getSharedPreferences("city_name",MODE_PRIVATE);
        city_name = sharedPreferences.getString("city","Hanoi");
        create_noti_weather(city_name);
        a = intent.getStringExtra("name");
        b = intent.getStringExtra("SDT");
        Intent intent21 = new Intent(this, chat_in.class);
        intent21.putExtra("namein", a);
        intent21.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent21, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("mynoti", "mynoti", NotificationManager.IMPORTANCE_HIGH);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
        //service
        {
//            Intent service_chat = new Intent(this, com.example.chat_tealtime.service_chat.class);
//            service_chat.putExtra("name",a);
//
//            startService(service_chat);
        }
        getSupportActionBar().setTitle("Menu  id: " + b + a);
        listView = (ListView) findViewById(R.id.list_item);
        arrayList = new ArrayList<>();
        arrayList.add("Chat");
        arrayList.add("Video_Social");
        arrayList.add("Đăng Video (Video_Social)");
        arrayList.add("Game (Beta)");

        ArrayAdapter arrayAdapter = new ArrayAdapter(Main_Screen.this, android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                    Intent intent = new Intent(Main_Screen.this, chat.class);
                    intent.putExtra("name", a);
                    intent.putExtra("SDT", b);
                    startActivity(intent);

                }
                if (position == 1) {

                    Intent intent = new Intent(Main_Screen.this, VIdeo_Social.class);
                    intent.putExtra("name", a);
                    intent.putExtra("SDT", b);
                    startActivity(intent);

                }
                if (position == 2) {

                    Intent intent = new Intent(Main_Screen.this, Dang_video.class);
                    intent.putExtra("name", a);
                    intent.putExtra("SDT", b);
                    startActivity(intent);

                }
                if (position == 3) {

                    Intent intent = new Intent(Main_Screen.this, game_in.class);
                    intent.putExtra("name", a);
                    intent.putExtra("SDT", b);
                    startActivity(intent);

                }

            }
        });
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("chat").child("All").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                mess_noidung messNoidung = snapshot.getValue(mess_noidung.class);

                // tao thong bao
                if (System.currentTimeMillis() - messNoidung.time < 5000) {// khoảng time để xác định thông báo mới

                    if (a.equals(messNoidung.name) == false) {
                        builder.setContentTitle("Chat");
                        builder.setContentText(messNoidung.name + " : " + messNoidung.text);
                        builder.setSmallIcon(R.drawable.ic_message);
                        builder.setAutoCancel(true);
                        builder.setContentIntent(pendingIntent);
                        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(Main_Screen.this);
                        managerCompat.notify(1, builder.build());

                    }
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.memu_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            Intent intent = new Intent(Main_Screen.this, MainActivity.class);
            intent.putExtra("save", 1);

            this.finish();
            startActivity(intent);
        }
        if (item.getItemId()== R.id.caidat){
            Dialog dialog= new Dialog(this);
            dialog.setContentView(R.layout.layout_setup);
            txtname_city = (EditText) dialog.findViewById(R.id.city_name);
            btn_luucaidat = (Button) dialog.findViewById(R.id.luucaidat);
            btn_thoatcaidat = (Button) dialog.findViewById(R.id.thoat_caidat);
            btn_thoatcaidat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btn_luucaidat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    city_name = txtname_city.getText().toString();
                    create_noti_weather(city_name);
                    SharedPreferences.Editor editor= sharedPreferences.edit();
                    editor.putString("city",city_name);
                    editor.commit();
                    dialog.dismiss();
                }
            });
            txtname_city.setText(city_name);

            dialog.show();
            Log.i("city",city_name);

        }

        return super.onOptionsItemSelected(item);
    }

    //    @Override
//    protected void onStop() {
//        super.onStop();
//        Toast.makeText(Main_Screen.this,"OK",Toast.LENGTH_SHORT).show();
//        CountDownTimer countDownTimer = new CountDownTimer(10000,1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                //Toast.makeText(Main_Screen.this,millisUntilFinished+"",Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onFinish() {
//               // Toast.makeText(Main_Screen.this,"Xong"+"",Toast.LENGTH_SHORT).show();
//
//            }
//        };
//        countDownTimer.start();
//        Intent intent21 =new Intent(this,chat_in.class);
//        intent21.putExtra("namein",a);
//        intent21.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent =PendingIntent.getActivity(this,0,intent21,PendingIntent.FLAG_UPDATE_CURRENT);
//        databaseReference = FirebaseDatabase.getInstance().getReference();
//        databaseReference.child("chat").child("All").addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//                mess_noidung messNoidung = snapshot.getValue(mess_noidung.class);
//
//                // tao thong bao
//                if (System.currentTimeMillis() - messNoidung.time<5000) {// khoảng time để xác định thông báo mới
//
//                    if (a.equals(messNoidung.name) == false) {
//                        builder.setContentTitle("Chat");
//                        builder.setContentText(messNoidung.name + " : " + messNoidung.text);
//                        builder.setSmallIcon(R.drawable.ic_message);
//                        builder.setAutoCancel(true);
//                        builder.setContentIntent(pendingIntent);
//                        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//                        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(Main_Screen.this);
//                        managerCompat.notify(1, builder.build());
//
//                    }
//                }
//
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });
//    }
    private class weather extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            URL url = null;
            String content = "";
            try {
                url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                int data = inputStreamReader.read();

                char ch;
                while (data != -1) {
                    ch = (char) data;
                    content = content + ch;
                    data = inputStreamReader.read();

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return content;
        }
    }

    private void create_noti_weather(String city) {
        String content, content1;
        String status = "", description = "", temp = "", name = "";
        double tempc = 0;
        weather w = new weather();
        try {
            content = w.execute("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=83a429520e15d0aed4325df0e0206c0c&lang=vi").get();
            Log.i("content", content);
//            content1 = w.execute("https://www.accuweather.com/vi/vn/hanoi/353412/current-weather/353412").get();
//
//            Log.i("AAAAA", content1);
            JSONObject jsonObject = new JSONObject(content);
            String weatherdata = jsonObject.getString("weather");
            String weatherdata1 = jsonObject.getString("main");
            name = jsonObject.getString("name");

            JSONArray jsonArray = new JSONArray(weatherdata);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                status = object.getString("main");
                description = object.getString("description");
            }

            JSONObject jsonObject1 = new JSONObject(weatherdata1);
            temp = jsonObject1.getString("temp");
            tempc = Double.parseDouble(temp);
            tempc = tempc - 273.15;


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        builder1.setContentTitle("Thời tiết");
        builder1.setContentText(name + "\n" + description + "\n" + "Nhiệt độ : " + tempc + "°C");
        builder1.setSmallIcon(R.drawable.ic_weather);
        builder1.setAutoCancel(true);
        builder1.setStyle(new NotificationCompat.BigTextStyle().bigText(name + "\n" + description + "\n" + "Nhiệt độ : " + tempc + "°C"));
        builder1.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(Main_Screen.this);
        managerCompat.notify(1, builder1.build());
    }

    void set_click_welcome(){
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://web27-c1172.web.app/New%20folder%20(2)/to%20tinh/to%20tinh/index.html"));
                startActivity(intent);


            }
        });

    }

    @Override
    public void onBackPressed() {// dont destroy app when press back (API>6>
        moveTaskToBack(true);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {// dont destroy app when press back (API<6>
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}







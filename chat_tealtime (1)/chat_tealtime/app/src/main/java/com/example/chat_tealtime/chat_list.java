package com.example.chat_tealtime;

public class chat_list {
    private String name;
    private String status;
    private int img;

    public chat_list(String name, String status, int img) {
        this.name = name;
        this.status = status;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}

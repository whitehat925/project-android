package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class chat extends AppCompatActivity {
    ListView listView;
    chat_list_adapter chat_list_adapter;
    ArrayList<chat_list> arrayList;
    String na,sdt;
    int kt =0;
//    DatabaseReference databaseReference;
//    NotificationCompat.Builder builder = new NotificationCompat.Builder(chat.this,"mynoti");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Intent intent = getIntent();
        na =intent.getStringExtra("name");
        sdt = intent.getStringExtra("SDT");

        // event click thong bao
//        Intent intent21 =new Intent(this,chat_in.class);
//        intent21.putExtra("namein",na);
//        intent21.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent =PendingIntent.getActivity(this,0,intent21,PendingIntent.FLAG_UPDATE_CURRENT);
//
//            if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.O)
//            {
//                NotificationChannel channel = new NotificationChannel("mynoti","mynoti", NotificationManager.IMPORTANCE_HIGH);
//                NotificationManager manager = getSystemService(NotificationManager.class);
//                manager.createNotificationChannel(channel);
//            }


        // đặt tên tiêu đề activity
        getSupportActionBar().setTitle("Chat "+na);
        listView = (ListView) findViewById(R.id.list_chat);
        arrayList = new ArrayList<>();
        arrayList.add(new chat_list("@All","ready",R.drawable.all));
        chat_list_adapter = new chat_list_adapter(this,R.layout.line_chat,arrayList);
        listView.setAdapter(chat_list_adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0)
                {
                    Intent intent1 = new Intent(chat.this,chat_in.class);
                    intent1.putExtra("SDT",sdt);
                    intent1.putExtra("name",na);
                    startActivity(intent1);
                }
            }
        });

        // firebase database
//        databaseReference = FirebaseDatabase.getInstance().getReference();
//            databaseReference.child("chat").child("All").addChildEventListener(new ChildEventListener() {
//                @Override
//                public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//                    mess_noidung messNoidung = snapshot.getValue(mess_noidung.class);
//
//                    // tao thong bao
//                    if (na.equals(messNoidung.name) == false) {
//                        builder.setContentTitle("Chat");
//                        builder.setContentText(messNoidung.name + " : " + messNoidung.text);
//                        builder.setSmallIcon(R.drawable.ic_message);
//                        builder.setAutoCancel(true);
//                        builder.setContentIntent(pendingIntent);
//                        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//                        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(chat.this);
//                        managerCompat.notify(1, builder.build());
//
//                    }
//
//                }
//
//                @Override
//                public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//
//                }
//
//                @Override
//                public void onChildRemoved(@NonNull DataSnapshot snapshot) {
//
//                }
//
//                @Override
//                public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError error) {
//
//                }
//            });

    }

}
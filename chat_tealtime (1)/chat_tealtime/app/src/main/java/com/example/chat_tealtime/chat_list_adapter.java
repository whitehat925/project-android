package com.example.chat_tealtime;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class chat_list_adapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<chat_list> list;

    public chat_list_adapter(Context context, int layout, List<chat_list> list) {
        this.context = context;
        this.layout = layout;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(layout,null);
        TextView txtname = (TextView) convertView.findViewById(R.id.txtname);
        TextView txtstatus = (TextView) convertView.findViewById(R.id.txtstatus);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imgchat);
        chat_list chatList = list.get(position);
        txtname.setText(chatList.getName());
        txtstatus.setText(chatList.getStatus());
        imageView.setImageResource(chatList.getImg());
        return convertView;
    }
}

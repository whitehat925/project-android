package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class VIdeo_Social extends AppCompatActivity {
    ViewPager2 viewPager2;
    public static int like = 0;
    TextView txtdangvideo;
    String name, SDT;
    DatabaseReference databaseReference;
    List<videoitem> videoitems = new ArrayList<>();
    List<videoitem> videoitemsload = new ArrayList<>();
    SwipeRefreshLayout refreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_v_ideo__social);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        SDT = intent.getStringExtra("SDT");
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refesh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {// làm mới video
            @Override
            public void onRefresh() {

                Toast.makeText(VIdeo_Social.this, "Làm mới", Toast.LENGTH_SHORT).show();
                refreshLayout.setRefreshing(true);
                int s = videoitems.size() - 1;

                // Thay list từ dưới lên đầu
                for (int i = s; i >= 0; i--) {

                    videoitemsload.add(new videoitem(videoitems.get(i).Videourl, videoitems.get(i).video_name, videoitems.get(i).video_text));
                }

                viewPager2.setAdapter(new Video_Apapter(videoitemsload));
                refreshLayout.setRefreshing(false);
            }
        });

        viewPager2 = (ViewPager2) findViewById(R.id.videoviewpaper);
        videoitem videoitem1 = new videoitem("https://firebasestorage.googleapis.com/v0/b/tets-c58bb.appspot.com/o/All%2F1612498446272?alt=media&token=923b7bb1-91d4-4a8d-9477-8428171129bb", "Girl", "Hi!!!");
        videoitem videoitem2 = new videoitem("https://firebasestorage.googleapis.com/v0/b/tets-c58bb.appspot.com/o/Video_Social%2F368cc53d77105e25f0437a164f5c4888.mp4?alt=media&token=f08e5e86-b107-48bf-b18a-56f74fd99eb9", "Hello", "Hello World");
        videoitems.add(videoitem2);
        videoitems.add(videoitem1);
        load_video();
        viewPager2.setAdapter(new Video_Apapter(videoitems));
        tb();
    }

    private void load_video() {// load all data video trên database
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Video_Social").child("All").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                video_upload_social v = snapshot.getValue(video_upload_social.class);

                videoitem v1 = new videoitem(v.url, v.name, v.text);
                videoitems.add(v1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    void tb() {// kết hợp vs function bên trong class videoadapter để lấy event double click(có thể thay bằng onclick)
        CountDownTimer countDownTimer = new CountDownTimer(1000000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (like == 1) {
                    Toast.makeText(VIdeo_Social.this, "Đã thích", Toast.LENGTH_SHORT).show();
                    like = 0;
                }
            }

            @Override
            public void onFinish() {
            }
        };
        countDownTimer.start();
    }
}
package com.example.chat_tealtime;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

public class img_chat_thuvien_adpater extends BaseAdapter {
    Context context;
    int layout;
    List<chat_thuvien> chatThuviens;

    public img_chat_thuvien_adpater(Context context, int layout, List<chat_thuvien> chatThuviens) {
        this.context = context;
        this.layout = layout;
        this.chatThuviens = chatThuviens;
    }

    @Override
    public int getCount() {
        return chatThuviens.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private  class ViewHolder{
        ImageView hinh;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder ;
        if (convertView== null){
            holder = new ViewHolder();
            LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(layout,null);
            holder.hinh = (ImageView) convertView.findViewById(R.id.thuvien_img);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        chat_thuvien chat_thuvien = chatThuviens.get(position);

        holder.hinh.setImageBitmap(chat_thuvien.getHinh());
        return convertView;
    }
}

package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class Dang_video extends AppCompatActivity {

    FirebaseStorage storage1;
    FirebaseDatabase database1;
    String name,SDT;
    Button btn_themvideo,btn_dang;
    EditText txt_mota;
    ProgressBar progressBar;
    Uri uriaddfile=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_video);
        Intent intent = getIntent();
        name =intent.getStringExtra("name");
        SDT = intent.getStringExtra("SDT");
        btn_dang =(Button) findViewById(R.id.btn_dang);
        btn_dang.setActivated(false);
        progressBar =(ProgressBar) findViewById(R.id.load_dang);
        progressBar.setVisibility(View.INVISIBLE);
        storage1 = FirebaseStorage.getInstance();
        database1 = FirebaseDatabase.getInstance();
        txt_mota = (EditText) findViewById(R.id.mota_video);
        btn_themvideo =(Button) findViewById(R.id.btn_themvideo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btn_themvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Themvideo();
            }
        });


        setBtn_dang();


    }
    private void Themvideo(){


                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("video/mp4");
                    startActivityForResult(intent,27);
                    //btn_dang.setActivated(true);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case 27:
                if(resultCode==RESULT_OK){


                    uriaddfile= data.getData();




                }
                break;

        }


    }
    protected  void setBtn_dang(){
        btn_dang.setOnClickListener(new View.OnClickListener() {

            String filename= System.currentTimeMillis()+"";
            @Override
            public void onClick(View v) {
                if (uriaddfile!=null) {
                    progressBar.setVisibility(View.VISIBLE);

                    StorageReference storageReference = storage1.getReference();

                    UploadTask uploadTask = storageReference.child("Video_Social").child(filename).putFile(uriaddfile);
                    uploadTask.addOnSuccessListener(Dang_video.this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //Uri url = taskSnapshot.getDownloadUrl();
                            Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();
                            while(!uri.isComplete());
                            Uri url = uri.getResult();
                            DatabaseReference databaseReference= database1.getReference();
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(Dang_video.this, "Đã đăng", Toast.LENGTH_SHORT).show();
                            video_upload_social v = new video_upload_social(name,SDT,txt_mota.getText().toString(),url.toString());
                            databaseReference.child("Video_Social").child("All").push().setValue(v);




                        }
                    });


                }
                else if (uriaddfile==null){
                    Toast.makeText(Dang_video.this,"Bạn phải thêm Video trước!",Toast.LENGTH_LONG).show();
                }


            }

        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
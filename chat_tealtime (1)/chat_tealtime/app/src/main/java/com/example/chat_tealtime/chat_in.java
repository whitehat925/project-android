package com.example.chat_tealtime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class chat_in extends AppCompatActivity {

    DatabaseReference databaseReference;
    Button btnsend;
    ImageButton addfile;
    ImageView img;
    EditText txtnhapmess;
    TextView textViewchat,txtex;
    String name, sdt;
    ScrollView scrollView;
    GridView gridView;
    ArrayList<chat_thuvien> arrayList;
    img_chat_thuvien_adpater adpater;
    MediaPlayer mediaPlayer = new MediaPlayer();
    Uri myUri;
    ArrayList<String> uri_media;
    Menu mn;int p;
    TableLayout table_mess;

    //NotificationCompat.Builder builder = new NotificationCompat.Builder(chat_in.this,"mynoti");


    // Get a non-default Storage bucket
    FirebaseStorage storage1;
    FirebaseDatabase database1;
    Uri uriaddfile;TableRow.LayoutParams lp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_in);
        table_mess = (TableLayout) findViewById(R.id.txtchat1);
        txtex= (TextView) findViewById(R.id.txtex);
        btnsend = (Button) findViewById(R.id.btn_send);
        // img = (ImageView) findViewById(R.id.img);
        //textViewchat = (TextView) findViewById(R.id.txtchat);// đoạn chat
        txtnhapmess = (EditText) findViewById(R.id.txtmess); // nội dung
        addfile = (ImageButton) findViewById(R.id.addfile);// btn add file
        setAddfile();



        scrollView = (ScrollView) findViewById(R.id.scrollView);// cuốn dọc
        scrollView.fullScroll(View.FOCUS_DOWN);// cuốn xuống dưới
        getSupportActionBar().setTitle("Chat @All");// set tên của actionbar
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        sdt = intent.getStringExtra("SDT");
        Intent intent1 = getIntent();
        if (name == null) {
            name = intent1.getStringExtra("namein");

        }

//        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.O)
//        {
//            NotificationChannel channel = new NotificationChannel("mynoti","mynoti", NotificationManager.IMPORTANCE_HIGH);
//            NotificationManager manager = getSystemService(NotificationManager.class);
//            manager.createNotificationChannel(channel);
//        }


        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("chat").child("All").addChildEventListener(new ChildEventListener() {
            @Override// lấy data tin nhắn
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                mess_noidung messNoidung = snapshot.getValue(mess_noidung.class);
                if (messNoidung.link.equals("")) {// neeys link rỗng
                    // textViewchat.setText(textViewchat.getText().toString() + messNoidung.name + " : " + messNoidung.text + "\n \n");
                    if (messNoidung.name.equals(name)){


                        load_tablerow_mess_myself(messNoidung.text + "\n");
                    }
                    else
                    load_tablerow_mess(messNoidung.name + " : " + messNoidung.text + "\n");
                    scrollView.fullScroll(View.FOCUS_DOWN);
                } else {
                    // textViewchat.setText(textViewchat.getText().toString() + messNoidung.name + " : " + messNoidung.text + "\n \n");
                    if (messNoidung.name.equals(name)){
                        load_tablerow_mess_myself(messNoidung.text + "\n");
                    }
                    else
                        load_tablerow_mess(messNoidung.name + " : " + messNoidung.text + "\n");
                    {
                        //TableRow tableRow = new TableRow(chat_in.this);
//                        Bitmap bitmapImage = BitmapFactory.decodeFile(messNoidung.link);
//                        int nh = (int) ( bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()) );
//                        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);
//
//
//                        ImageView imageView= new ImageView(chat_in.this);
//                        imageView.setImageBitmap(scaled);
//                        imageView.setImageResource(R.drawable.card_2_bich);
//                        //imageView.requestLayout();
//                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
//                        imageView.setLayoutParams(layoutParams);
//                        imageView.setMaxHeight(20);
//                        imageView.setMaxWidth(20);

                        //imageView.setScaleType(ImageView.ScaleType.CENTER);

                        //tableRow.addView(imageView);
                        //table_mess.addView(imageView);
                    }
                   // new loadImage_mess().execute(messNoidung.link);// thêm ảnh vào mess
                    scrollView.fullScroll(View.FOCUS_DOWN);

                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        btnsend.setOnClickListener(new View.OnClickListener() {//gửi tin nhắn
            @Override
            public void onClick(View v) {
                mess_noidung messNoidung = new mess_noidung(name, txtnhapmess.getText().toString(), "", System.currentTimeMillis());
                databaseReference.child("chat").child("All").push().setValue(messNoidung);
                txtnhapmess.setText("");
                scrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {// tạo menu
        getMenuInflater().inflate(R.menu.memu_menu, menu);
        mn = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {// click item menu
        if (item.getItemId() == R.id.logout) {
            // trả về login để không bị auto login
            Intent intent = new Intent(chat_in.this, MainActivity.class);
            intent.putExtra("save", 1);
            this.finish();
            startActivity(intent);
        }
        if (item.getItemId() == R.id.thuvien) {// mở thư viên
            getthuvien();
        }
        if (item.getItemId() == R.id.tatnhac) {//tắt nhạc đang chạy
            mediaPlayer.stop();
            mn.getItem(0).setVisible(false);
            mn.getItem(4).setVisible(false);
        }
        if (item.getItemId() == R.id.play_pause) {// chơi nhạc hoặc dừng
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();

                item.setIcon(R.drawable.ic_play);
            } else {
                mediaPlayer.start();
                item.setIcon(R.drawable.ic_pasuse);
            }
        }
        return super.onOptionsItemSelected(item);

    }

    public void setAddfile() {// thêm file để gửi lên
        addfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storage1 = FirebaseStorage.getInstance();
                database1 = FirebaseDatabase.getInstance();


                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);// mở thư mục
                intent.setType("*/*");// lấy mọi loại
                startActivityForResult(intent, 27);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 27:
                if (resultCode == RESULT_OK) {

                    String filename = System.currentTimeMillis() + "";
                    uriaddfile = data.getData();
                    if (uriaddfile != null) {

                        StorageReference storageReference = storage1.getReference();
                        UploadTask uploadTask = storageReference.child("All").child(filename).putFile(uriaddfile);
                        uploadTask.addOnSuccessListener(chat_in.this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {// gửi file chọn nên server
                                //Uri url = taskSnapshot.getDownloadUrl();
                                Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl();// lấy url để download
                                while (!uri.isComplete()) ;
                                Uri url = uri.getResult();

                                Toast.makeText(chat_in.this, "Đã gửi", Toast.LENGTH_SHORT).show();
                                mess_noidung messNoidung = new mess_noidung(name, "Đã đính kèm tệp", url.toString(), System.currentTimeMillis());
                                databaseReference.child("chat").child("All").push().setValue(messNoidung);
                                scrollView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        scrollView.fullScroll(View.FOCUS_DOWN);
                                    }
                                });
                            }
                        });
                        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {
                                    throw task.getException();
                                }

                                // Continue with the task to get the download URL
                                return storageReference.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    Uri downloadUri = task.getResult();
                                    String downloadURL = downloadUri.toString();
                                    txtnhapmess.setText(downloadURL);
                                } else {
                                    // Handle failures
                                    // ...
                                }
                            }
                        });
                    }

                }
                break;

        }


    }

    void load_tablerow_mess(String mess) {// thêm 1 dòng takle tin nhắn mới
        TableRow tableRow = new TableRow(this);
        //tableRow.setBackgroundColor(Color.BLUE);
        TextView textView = new TextView(this);
        textView.setText(mess);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(20);
        tableRow.addView(textView);
        table_mess.addView(tableRow);
    }
    void load_tablerow_mess_myself(String mess) {// thêm 1 dòng takle tin nhắn mới
        //LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT);
        //layoutParams.weight=1f;
        //TableRow tableRow = new TableRow(this);
        //tableRow.setBackgroundColor(Color.GRAY);
        TextView textView = new TextView(this);
        //textView.setLayoutParams(layoutParams);
        textView.setText(mess);
        textView.setBackgroundColor(Color.parseColor("#454541"));
        textView.setGravity(Gravity.RIGHT);

        //textView.setWidth(0);
        //textView.setHeight(0);
        //textView.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        //textView.setLayoutParams(new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 0));
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(20);
        //tableRow.addView(textView);
        table_mess.addView(textView);
    }

    void load_tablerow_mess_img(Bitmap url) {// thêm dòng ima
        // TableRow tableRow = new TableRow(this);
//        TextView textView= new TextView(this);
//        textView.setText(mess);
//        textView.setTextColor(Color.WHITE);
//        textView.setTextSize(20);
//        tableRow.addView(textView);
        ImageView imageView = new ImageView(this);


        imageView.setImageBitmap(url);
        //tableRow.setLayoutParams(new TableRow.LayoutParams(30,30));
//        Display display = getWindowManager().getDefaultDisplay();
//        int width = display.getWidth(); // ((display.getWidth()*20)/100)
//        int height = display.getHeight();// ((display.getHeight()*30)/100)
//        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width,height);
//        imageView.setLayoutParams(parms);
        //tableRow.addView(imageView);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(10, 10);
//                        imageView.setLayoutParams(layoutParams);
        table_mess.addView(imageView);


    }

    public class loadImage_thuvien extends AsyncTask<String, Void, Bitmap> {// AsyncTask load ảnh

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;
            try {
                URL url = new URL(strings[0]);
                InputStream inputStream = url.openConnection().getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            super.onPostExecute(bitmap);
            arrayList.add(new chat_thuvien(bitmap));
            gridView.setAdapter(adpater);

        }
    }

    public class loadImage_mess extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;
            try {
                URL url = new URL(strings[0]);
                InputStream inputStream = url.openConnection().getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            super.onPostExecute(bitmap);
            load_tablerow_mess_img(bitmap);


        }
    }

    private void getthuvien() {// lấy ảnh được load từ AsyncTask qua link lấy trên server chat

        Bitmap Icon = BitmapFactory.decodeResource(getResources(), R.drawable.all);
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_chat_image);
        gridView = (GridView) dialog.findViewById(R.id.gridview_img);
        arrayList = new ArrayList<>();
        uri_media = new ArrayList<>();

        // add img
        databaseReference.child("chat").child("All").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                mess_noidung messNoidung = snapshot.getValue(mess_noidung.class);
                if (messNoidung.link.equals("")) {
                } else {
                    new loadImage_thuvien().execute(messNoidung.link);
                    uri_media.add(new String(messNoidung.link));


                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        // zoom img khi click
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Dialog dialog1 = new Dialog(dialog.getContext());
                 p=position;
                dialog1.setContentView(R.layout.click_img_chat);
                ImageView img_click = (ImageView) dialog1.findViewById(R.id.click_img);
                class  gess extends GestureDetector.SimpleOnGestureListener{// class vuot man hih
                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                        int s= arrayList.size();

                        if (e2.getX()-e1.getX()>100 && Math.abs(velocityX)>100)//lui aanh
                        {
                            Log.i("vuot","ok");
                            img_click.setImageBitmap(arrayList.get(p-1).getHinh());
                            p--;
                            Log.i("vii",p+""+s);
                            if (p<=0){ p=s;}

                        }
                        if (e1.getX()-e2.getX()>100 && Math.abs(velocityX)>100)// tien anh
                        {
                            Log.i("vuot","ok");
                            img_click.setImageBitmap(arrayList.get(p+1).getHinh());
                            p++;
                            Log.i("vii",p+""+s);
                            if (p>=s-1){ p=-1;}

                        }
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                };
                GestureDetector gestureDetector= new GestureDetector(dialog1.getContext(),new gess());


                img_click.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        gestureDetector.onTouchEvent(event);
                        return true;
                    }
                });
                img_click.setImageBitmap(arrayList.get(position).getHinh());
                myUri = Uri.parse(uri_media.get(position).toString());
                try {// meu la file mp3,mp4

                    mediaPlayer.setDataSource(dialog.getContext(), myUri);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.prepare(); //don't use prepareAsync for mp3 playback
                    // hien thi cong cu choi nhac
                    mn.getItem(0).setVisible(true);
                    mn.getItem(4).setVisible(true);
                    mn.getItem(0).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                    mediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                dialog1.show();
            }
        });
        // arrayList.add(new chat_thuvien(Icon));
        adpater = new img_chat_thuvien_adpater(dialog.getContext(), R.layout.line_img_chat_thuvien, arrayList);
        //gridView.setAdapter(adpater);
        dialog.show();

    }



}
//package com.example.chat_tealtime;
//
//import android.app.Notification;
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.Intent;
//import android.media.RingtoneManager;
//import android.os.IBinder;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.core.app.NotificationCompat;
//import androidx.core.app.NotificationManagerCompat;
//
//import com.google.firebase.database.ChildEventListener;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//
//public class service_chat extends Service {
//    DatabaseReference reference;
//    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "mynoti");
//
//
//    @Override
//    public void onDestroy() {
//        //Toast.makeText(this,"Stop",Toast.LENGTH_SHORT).show();
//        super.onDestroy();
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        //Toast.makeText(this,"Start",Toast.LENGTH_SHORT).show();
//        String a = intent.getStringExtra("name");
//        Intent intent21 = new Intent(this, chat_in.class);
//        intent21.putExtra("namein", a);
//        intent21.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent21, PendingIntent.FLAG_UPDATE_CURRENT);
//        reference = FirebaseDatabase.getInstance().getReference();
//        reference.child("chat").child("All").addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//                mess_noidung messNoidung = snapshot.getValue(mess_noidung.class);
//                // tao thong bao
//                if (System.currentTimeMillis() - messNoidung.time < 5000) {// khoảng time để xác định thông báo mới
//
//                    if (a.equals(messNoidung.name) == false) {
//                        builder.setContentTitle("Chat");
//                        builder.setContentText(messNoidung.name + " : " + messNoidung.text);
//                        builder.setSmallIcon(R.drawable.ic_message);
//                        builder.setAutoCancel(true);
//                        builder.setContentIntent(pendingIntent);
//                        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//                        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(service_chat.this);
//                        managerCompat.notify(1, builder.build());
//
//                    }
//                }
//
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });
//
//        //startForeground(1,no);
//        return START_STICKY;
//    }
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//}

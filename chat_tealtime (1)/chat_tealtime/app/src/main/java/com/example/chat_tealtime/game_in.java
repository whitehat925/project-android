package com.example.chat_tealtime;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class game_in extends AppCompatActivity {
    ListView listView_game;
    ArrayList<String> arrayList;
    String name, SDT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_in);
        getSupportActionBar().setTitle("Game");
        Intent getintent = getIntent();
        name = getintent.getStringExtra("name");
        SDT = getintent.getStringExtra("SDT");
        listView_game = (ListView) findViewById(R.id.listview_game);
        arrayList = new ArrayList<>();
        arrayList.add("Cờ Caro Online");
        arrayList.add("3 Cây Online");
        ArrayAdapter adapter = new ArrayAdapter(game_in.this, android.R.layout.simple_list_item_1, arrayList);
        listView_game.setAdapter(adapter);
        listView_game.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(game_in.this, game_caroonline.class);
                    intent.putExtra("name", name);
                    intent.putExtra("SDT", SDT);
                    startActivity(intent);
                }
                if (position == 1) {
                    Intent intent = new Intent(game_in.this, game_3cayonline_activity.class);
                    intent.putExtra("name", name);
                    intent.putExtra("SDT", SDT);
                    startActivity(intent);
                }

            }
        });
    }
}






